from cmd import Cmd
import os           # importamos librerias que nos servira mas adelante
import shutil
import subprocess               
import errno
import sys
from ftplib import FTP
import time

     #llamamos a cmd para definir los comandos
class MyPrompt(Cmd):
    prompt = 'SO> '
    intro = "Bienvenido a la shell de FABRIZIO Y OLIVA, Escriba HELP para mas ayuda"

    logging.basicConfig(filename='/var/log/personal_horarios_log.log', format='%(asctime)s %(message)s', level=logging.DEBUG )  #Aplicamos el formato de fechas 
        #y dirigimos a que archivo se va guardar         
    logging.info('Inicio de sesion ' %s) #Lo que hace logging es captar que el usuario entre con algun usuario a la shell y lo guarda en el archivo
    #con el parametro de las fechas y la palabra Inicio de sesion    

    #logout.basicConfig(filename='/var/log/personal_horarios_log.log', format='%(asctime)s %(message)s', level=logging.DEBUG)
    #logout.info('Cerró sesion &useradd ')

    def logshell(self,com,inp):
        #uno de los requerimientos es que cada comando listado vaya a la carpeta log /var/log
        sh = open("/var/log/lfs_log","a")   #abre el archivo en la carpeta /var/log
        hora = time.strftime("%y-%m-%d %H:%M:%S")   #deberia mostrar el horario
        sh.write(hora+" "+com+" "+inp+"\n") #Escribe los parametros de fecha y hora
        sh.close() #cierra el archivo
    #comando renom un archivo rename
    def do_renom(self, inp):
        self.logshell("rename",inp) #llama a la funcion logshell y escribe rename y lo escrito en teclado para saber los parametros
        inp = inp.split()   #este comando funciona igual que el comando move, ya que solamente emula moviendo
        try:
            #Mueve recursivamente el archivo a otra ubicacion que el usuarios nombra, simulando el cambio de nombre
            shutil.move(inp[0], inp[1])
        except:
            print ("El nombre no es valido o el archivo no existe")    
    #comando para moverse archivos
    def do_mover(self, inp):
        self.logshell("move",inp) #Separara las cadenas en n cadenas a traves de una llave recibiendo un string de por paso
        #recibe in string y separa las cadenas en n cadenas a traves de una llave
        inp = inp.split()   
        try:
            #Mueve el arhivo deseado a otra locacion
            shutil.move(inp[0], inp[1])     
        except:
            print ("Las rutas no son validas o el archivo no existe")  #aca abrimos el archivo log para escribir el error que le sale al usuario
            sh = open("/var/log/errores_sistema_log","a")                            
            error = str.format("Las rutas estan mal escritas o el archivo no existe ")   
            sh.write(error+" " "\n")
            sh.close() 
    #Comando Crear un directorio
    def do_crdir(self, inp):
        self.logshell("createdir",inp)
        inp = inp.split() #lo que recibe por teclado hace un split (separar) y ordena cuando encuentra un espacio
        try:
            #makedirs realiza una llamada al sistema de la funcion mkdir
            os.makedirs(inp[0])
        except:
            print ("El archivo ya existe o la ruta ingresada no es valida")

    #Comando Cambiar los propietarios de un archivo o un grupo de archivos
    def do_cambiarprop(self, inp):
        self.logshell("changeprop",inp)
        inp = inp.split()
        try:
            shutil.chown(inp[0], inp[1], inp[2]) #Realiza una llamada el comando chown cambian propietario de la ruta dada
        except:
            print ("No se pudo cambiar de propietario, error")

    #Comando Cambiar los permisos sobre un archivo o un grupo de archivos
    def do_cambiarper(self, inp):
        self.logshell("changeper",inp)
        inp = inp.split()
        inp[1] = int(inp[1], 8)#colocando el 8 como segundo parametro hace que la cadena sea en octal
        os.chmod(inp[0], inp[1])#Realiza una llamada al sistema a la funcion chmod

    #Comando para cambiar la contraseña
    def do_cambpass(self, inp):
        self.logshell("changepasswd",inp)
        inp = inp.split()
        try:
            #pasa la pregunta a la shell principal
            os.system("passwd "+inp[0])
        except:
            print ("No se ha podido cambiar la contrasena")

    #Comando copiar 
    #no se puede llamar al sistema
    def do_copiar(self, inp):
        self.logshell("copy",inp)
        inp = inp.split()
        try:
            shutil.copytree(inp[0], inp[1])   #La funcion copytree copia recursivamente un conjunto de directorios devolviendo asi el directorio destino
            print("Se ha copiado el archivo o el directorio seleccionado")
        except IndexError: 
            print("No se ha podido copiar el archivo o el directorio seleccionado")
        except OSError as exc: #En caso que no se haya podido copiar el archivo o directorio se emite un mensaje donde indica que no se logro
            if exc.errno == errno.ENOTDIR:#Comprobamos si es o no un directorio
                #La funcion copy copia un archivo
                shutil.copy(inp[0], inp[1])
            else: 
                raise #indicamos el tipo de excepcion que queremos lanzar de forma default

    #Comando Listar directorios.No se puede hacer una llamada al sistema
    def do_listar(self, inp):
        self.logshell("listd",inp)
        inp = inp.split()
        try:  #La funcion listdir muestra un listado de las carpetas en la ruta actual
            path = inp[0]   #path es el directorio que le indiquemos, se guarda en input
            dirs = os.listdir( path ) 
            for file in dirs: #HAce una especie de for para que vaya imprimiendo uno debajo del otro los dirs
                print (file)
        except:
            print("Por favor ingrese una ruta o la ruta ingresada no existe")

    #Comando para cambiar directorio (no se puede hacer una llamada al sistema de la funcion cd)
    def do_cambiardir(self, inp):
        self.logshell("changedir",inp)
        inp = inp.split()
        try:
            #La funcion cambia de directo
            os.chdir(inp[0])  #busca el directorio introducido por teclado y llama a os para ver si es correcto
            print (os.getcwd())
        except:
            print ("No se pudo cambiar de directorio o no existe el directorio seleccionado")

    #Comando para agregar usuario
    def do_agguser(self, inp):
        self.logshell("adduser",inp)
        inp = inp.split()
        try:
            #Realiza una llamada a la funcion useradd con os
            #el horario de entrada y salida :/ y por ultimo el ip | si no se sabe la ip introducir ip addr
            os.system("useradd -c " + "\"" + "horario = " + inp[1] + " ip = " + inp[2] + "\" " + inp[0])
        except:
            print ("No se pudo crear el usuario o se introdujo algun caracter no permitido")
    #Comando para imprimir los procesos 
    #imprime un número consecutivo cada segundo seguido de la fecha y hora a la que lo ha hecho
    def do_demon(self, inp):
        counter = 0
        while True:
        counter += 1 #mientras sea true el while counter sirve como contador de cuantas veces se imprime numeros consecutivos con fecha y de lo que hace
        print('{0} - {1}'.format(counter, datetime.datetime.now()))
        time.sleep(1) #tiempo que tarda en salir el print
    #Comando para salir de la shell                   
    def do_exit(self, inp):
        print("Tu has salido de la shell")
        return True
    #Ejecutar los comandos del sistema del bash
    def default(self, inp):
        #llama a las funciones que la shell no tiene declaradas
        subprocess.run(inp, shell=True, executable="/bin/bash") 
        p = subprocess.Popen(["scp", "my_file.txt", "username@server:path"]) #llama a un subprocces para poder realizar una transferencia scp
        sts = os.waitpid(p.pid, 0)  #Agarra la entrada de los parametros de scp como pasamanos a esta shell
     
    do_EOF = do_exit
    help_EOF = help_exit

MyPrompt().cmdloop()
